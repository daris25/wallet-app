import {Platform, Text} from 'react-native';
import {Navigation} from 'react-native-navigation';

import {registerScreens} from './src/app/control';

import Provider from './src/utils/MobxRnnProvider';
import Stores from './src/stores'
import AuthStore from './src/stores/auth'

registerScreens(Stores, Provider);
Text.allowFontScaling = false
console.disableYellowBox = true;

Navigation.events().registerAppLaunchedListener(async () => {
  // const login = await AuthStore.isLogged()
  // if (login.login) {
      Navigation.setRoot({
        root: {
          stack: {
            id: 'Login',
            children: [
              {
                component: {
                  name: 'Login',
                  options: {
                    topBar: {
                      visible: false,
                      height: 0
                    }
                  }
                }
              }
          ],
        },
        options: {
            statusBar: {
              backgroundColor: '#3e416e',
              visible: true
            },
          }
        }
      });
  // } else {
  //     Navigation.setRoot({
  //       root: {
  //         stack: {
  //           id: 'Login',
  //           children: [
  //             {
  //               component: {
  //                 name: 'Login',
  //                 options: {
  //                   topBar: {
  //                     visible: false,
  //                     height: 0
  //                   }
  //                 }
  //               }
  //             }
  //         ],
  //         options: {
  //             statusBar: {
  //               backgroundColor: '#3e416e',
  //               visible: true
  //             },
  //           }
  //         }
  //       }
  //     });
  // } 

    
});
