import {Navigation} from 'react-native-navigation';

import Login from '../screens/Login'
import Tabs from '../screens/Tabs'
import Home from '../screens/Home'
import News from '../screens/News'
import Wallet from '../screens/Wallet'
import Profile from '../screens/Profile'
import Transaction from '../screens/Transaction'
import Cash from '../screens/Cash'
import Program from '../screens/Program'
import Regis from '../screens/Regis'

import PinInput from '../pin/PinInput'
// import PinKeyboard from '../pin/PinKeyboard'
// import PinSreen from '../pin/PinSreen'



export function registerScreens(store: {}, Provider: {}) {

	Navigation.registerComponentWithRedux('Login', () => Login, Provider, store)
	Navigation.registerComponentWithRedux('Regis', () => Regis, Provider, store)

	Navigation.registerComponentWithRedux('Tabs', () => Tabs, Provider, store)
			Navigation.registerComponentWithRedux('Home', () => Home, Provider, store)
				Navigation.registerComponentWithRedux('News', () => News, Provider, store)
			Navigation.registerComponentWithRedux('Wallet', () => Wallet, Provider, store)
				Navigation.registerComponentWithRedux('Transaction', () => Transaction, Provider, store)
				Navigation.registerComponentWithRedux('Cash', () => Cash, Provider, store)
				Navigation.registerComponentWithRedux('Program', () => Program, Provider, store)
			Navigation.registerComponentWithRedux('Profile', () => Profile, Provider, store)

	Navigation.registerComponentWithRedux('PinInput', () => PinInput, Provider, store)
	// Navigation.registerComponentWithRedux('PinKeyboard', () => PinKeyboard, Provider, store)
	// Navigation.registerComponentWithRedux('PinSreen', () => PinSreen, Provider, store)

}

 