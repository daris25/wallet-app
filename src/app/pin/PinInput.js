'use strict';

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  Image,
  ImageBackground
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import { Navigation } from 'react-native-navigation';
import { PinKeyboard, PinInput } from 'react-native-awesome-pin';
import {setRoot} from '../control/Control';

class Program extends Component {
  constructor(props) {
    super(props);
  
    this.state = {
      pin1 : '',
      pin2 : '',
      pin3 : '',
      pin4 : '',
      pin5 : '',
      pin6 : '',
      pint : '',
      dots : 0

    };
  }



  recievePin(pin){
    const pins=pin
    console.log('cekpin', pins)

    if(this.state.pin1 == ''){
        this.setState({
          pin1: pin,
          dot: 1
        })
    }else if(this.state.pin2 == ''){
        this.setState({
          pin2: pin,
          dot: 2
        })
    }else if(this.state.pin3 == ''){
        this.setState({
          pin3: pin,
          dot: 3
        })
    }else if(this.state.pin4 == ''){
        this.setState({
          pin4: pin,
          dot: 4
        })
    }else if(this.state.pin5 == ''){
        this.setState({
          pin5: pin,
          dot: 5
        })
    }else if(this.state.pin6 == ''){
        this.setState({
          pin6: pin,
          dot: 6
        })

      const pinss = this.state.pin1.toString()+this.state.pin2.toString()+this.state.pin3.toString()+this.state.pin4.toString()+this.state.pin5.toString()+this.state.pin6.toString()
          console.log('pin gabung', pinss)

          if (pinss) {
            setRoot('Tabs',1)
          } else {
            Navigation.pop(this.props.componentId)
          }

        // setTimeout(() => {
        //   const pinss = this.state.pin1.toString()+this.state.pin2.toString()+this.state.pin3.toString()+this.state.pin4.toString()+this.state.pin5.toString()+this.state.pin6.toString()
        //   console.log('pin gabung', pinss)
        // }, 100)

    }else{
      console.log('pin sudah penuh')
    }

   // Clear error on interaction
   // this.pinScreen.clearError();
  
   // if(pin != '56771'){
   //     this.pinScreen.throwError('Your PIN is incorrect');
   // }
}

  render() {
    return (
  
        <ImageBackground  style={{width: '100%', height: '100%', justifyContent: 'space-between'}}>
          
            <ImageBackground style={{width: '100%', height: '51%', backgroundColor: 'grey'}}>
              <TouchableOpacity onPress={()=>Navigation.pop(this.props.componentId)}>  
                <View style={{flexDirection: 'row', padding: 15, justifyContent: 'space-between'}}>
                  <Icon name="arrowleft" style={{fontSize: 25, color: 'white'}}/>
                  <Text style={{fontSize: 15, color: 'white'}}>Masukan PIN</Text>
                </View>
              </TouchableOpacity>
                <Image source={require('../images/logo.png')} style={{width: 130, height: 130, borderRadius: 100, padding: 10, alignSelf: 'center', marginTop: 20}} />
            </ImageBackground>

             <View stye={{width: '100%', height: '50%'}}> 
                <View style={{backgroundColor: 'grey', width: '100%', alignItems: 'center'}}>
                    <PinInput
                        onRef={ref => (this.pinScreen = ref)}
                        numberOfPins={6}
                        numberOfPinsActive={this.state.dot}
                        pinActiveStyle={{backgroundColor: 'maroon'}}
                    />
                </View>
                <PinKeyboard
                    onRef={ ref => (this.pinScreen = ref) }
                    keyDown={ this.recievePin.bind(this) }
                />
            </View>

        </ImageBackground>

    );
  }
}

const styles = StyleSheet.create({

  viewHeader:{
    width: '100%',
    height: 50,
    backgroundColor: 'white',
    elevation: 5,
    marginBottom: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingRight: 15,
    paddingLeft: 15
  },

});


export default Program;