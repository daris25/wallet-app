import React, {Component} from 'react';
import {Platform, StyleSheet, View} from 'react-native';
import { Container, Tabs, Tab, TabHeading, Text } from 'native-base';
import Icon from 'react-native-vector-icons/AntDesign';

import Home from './Home';
import Wallet from './Wallet';
import Profile from './Profile';
import Pin from '../pin/PinInput';

export default class Fotter extends Component {
  constructor(props) {
    super(props);
    console.log('ini ptprs', props.text)

    if (props.text == 1) {
      var page = 1
    } else {
      var page = 0
    }
   
      this.state = {
        props: props.text,
        page: page
      };
  }

  componentDidMount() {
     setTimeout(this._tabs.goToPage.bind(this._tabs,this.state.page))
  }

  render() {
    return (
      <Container>
          <Tabs tabBarPosition='bottom' locked 
              tabBarUnderlineStyle= {{backgroundColor: 'maroon'}}
              ref={component => this._tabs = component} >
            <Tab heading={  <TabHeading style={{backgroundColor: 'white'}}>
                              <View style={{justifyContent: 'center', alignItems: 'center'}}>
                                <Icon name="appstore-o" style={{fontSize: 25}}/>
                                <Text style={{fontSize: 12, color: 'black'}}>Home</Text>
                              </View>
                            </TabHeading>} >
              <Home {...this.props}/>
            </Tab>
            <Tab heading={ <TabHeading style={{backgroundColor: 'white'}}>
                              <View style={{justifyContent: 'center', alignItems: 'center'}}>
                                <Icon name="tags" style={{fontSize: 25}}/>
                                <Text style={{fontSize: 12, color: 'black'}}>Wallet</Text>
                              </View>
                            </TabHeading>} >
              <Wallet {...this.props}/>
            </Tab>
            <Tab heading={ <TabHeading style={{backgroundColor: 'white'}}>
                              <View style={{justifyContent: 'center', alignItems: 'center'}}>
                                <Icon name="slack-square" style={{fontSize: 25}}/>
                                <Text style={{fontSize: 12, color: 'black'}}>Profile</Text>
                              </View>
                            </TabHeading>} >
              <Profile {...this.props}/>
            </Tab>
          </Tabs>
      </Container>
    );
  }
}

const styles = StyleSheet.create({


});
