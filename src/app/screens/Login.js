import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Image, ImageBackground} from 'react-native';
import { Container, Content, Item, Input, Button, Form, Label, Right, Left} from 'native-base';
import Icon from 'react-native-vector-icons/Entypo';
import { Navigation } from 'react-native-navigation';
import {PermissionsAndroid} from 'react-native';

import {setRoot, pushScreen} from '../control/Control';

export default class Home extends Component {
  render() {
    return (
      <Container>
        <Content>   

          <Text style={styles.text1}></Text>

          <View style={styles.viewLogo}>
            <Image style={styles.logo} source={require('../images/logo.png')} />
          </View>

          <View style={styles.viewForm}>
            <Form>
              <Item stackedLabel>
                <Label>Username</Label>
                <Input />
              </Item>
              <Item stackedLabel>
                <Label>Password</Label>
                <Input />
              </Item>
            </Form>
          </View>

          <View style={styles.viewButton}>
            <Left>
                <Button style={styles.regis} onPress={()=> pushScreen('Regis')}>
                    <Text style={styles.textLogin}>Registrasi</Text>
                </Button>
            </Left> 
            <Right style={styles.viewLogin}> 
                <Button style={styles.login} onPress={()=> setRoot('Tabs')}>
                    <Text style={styles.textLogin}>Login</Text>
                </Button>
            </Right>
          </View>

        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({

    text1:{
      fontSize: 30,
      fontWeight: 'bold',
      marginTop: 20,
      marginLeft: 30,
      marginBottom: 20,
      color: 'black'
    },

    viewLogo:{
      alignSelf: 'center',
      alignItems: 'center',
      width: 110,
      height: 110,
      backgroundColor: 'black',
      justifyContent: 'center',
      borderRadius: 100,
      marginBottom: 20,
      elevation: 10
    },
        logo:{
          height: '90%',
          width: '90%'
        },

    viewForm:{
      backgroundColor: 'white',
      width: '85%',
      paddingBottom: 20,
      alignSelf: 'center',
      paddingRight: 20,
      elevation: 10,

      borderRadius: 10,
      margin: 10
    },

    viewButton:{
      //backgroundColor: 'grey',
      flexDirection: 'row',
      width: '100%',
      height: 50,
      marginTop: 15,
      elevation: 5
    },
        buttonLogin:{
          //backgroundColor: 'blue',
          width: '100%',
          height: '100%'
        },
            login:{
              width: 150,
              borderTopLeftRadius: 10,
              borderBottomLeftRadius: 10,
              justifyContent: 'center'
            },
                textLogin:{
                  color: 'white',
                  fontWeight: 'bold',
                  fontSize: 14
                },
            regis:{
              width: 150,
              justifyContent: 'center',
              borderTopRightRadius: 10,
              borderBottomRightRadius: 10
            }

});
