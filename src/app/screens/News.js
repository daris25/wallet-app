import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Image, ImageBackground, TouchableOpacity} from 'react-native';
import { Container, Content, Icon, Item, Input, Button, Left, Right} from 'native-base';
import { Navigation } from 'react-native-navigation';

export default class Detail extends Component {
  render() {
    return (
      <Container> 
            <View style={styles.header}>
                <View style={{margin: 15}}>
                  <TouchableOpacity onPress={()=>Navigation.pop(this.props.componentId)}>
                    <Icon name="ios-arrow-back" />
                  </TouchableOpacity>
                </View>
                    <Text style={styles.textHeader}>Type a paper of Catalog Printing</Text>
            </View>
          <Content>
              <View style={styles.viewImg}>
                  <ImageBackground style={styles.backImg} source={require('../images/zakat.png')}>
                      <Text style={styles.textTitle}>Type a paper of Catalog Printing</Text>
                  </ImageBackground>
              </View>
              <View style={styles.viewText}>
                  <View style={styles.viewContent}>
                  <Text style={{textAlign: 'justify'}}>
                  
                  Manusia memang tidak luput dengan dosa. Kesempurnaannya dipertanyakan apakah kita pantas disebut makhluk yang sempurna padahal kita selalu enggan untuk meminta ampun dengan apa yang telah kita perbuat.

Nabi Muhammad صلى الله عليه وسلم bersabda, “Sedekah itu dapat menghapus dosa sebagaimana air itu memadamkan api“.(HR. At-Tirmidzi).

Sedekah, itulah cara mudah yang disediakan Allah agar dapat mengikis perbuatan-perbuatan dosa kita. Cukup dengan tersenyum saja, Anda sudah bersedekah karena senyum adalah salah satu sedekah termudah yang dapat kita sebarkan dengan mengukir garis senyum di bibir kita.

                  </Text>
                  </View>
              </View>
          </Content>
      </Container>
    
    );
  }
}

const styles = StyleSheet.create({
  header:{
    //backgroundColor: 'grey',
    width: '100%',
    height: 50,
    flexDirection: 'row',
    alignItems: 'center'
  },
        textHeader:{
          fontSize: 16,
          fontWeight: 'bold'
        },

  viewImg:{
    backgroundColor: 'red',
    width: '100%',
    height: 150,
  },
        backImg:{
          width: '100%',
          height: '100%',
          justifyContent: 'flex-end'
        },
              textTitle:{
                color: 'white',
                fontSize: 20,
                fontWeight: 'bold',
                margin: 10
              },

  viewText:{
    //backgroundColor: 'grey',
    marginTop: 10,
    marginBottom:10,
    width: '100%',
    //height: 450,
    justifyContent: 'center',
    alignItems: 'center'
  },
      viewContent:{
        backgroundColor: 'white',
        width: '95%',
        //height: '95%',
        elevation: 5,
        borderRadius: 10,
        padding: 15
      },
          textContent:{
            margin: 8,
            fontSize: 15,
            textAlign: 'justify'
          }

});
