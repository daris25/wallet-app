import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Image, ImageBackground, TouchableOpacity} from 'react-native';
import { Container, Content, Icon, Item, Input, Button,Form, Label} from 'native-base';
import { Navigation } from 'react-native-navigation';
import { PinScreen } from 'react-native-awesome-pin';
import {pushScreen} from '../control/Control';

export default class Detail extends Component {

  constructor(props) {
    super(props);
    console.log('wallet',props)

    if (props.text == 1) {
      var key = 123456
    } else {
      var key =''
    }

    this.state = {
      key: key
    };
  }

  async componentDidMount() {
    
  }

  _renderItems() {
    return (
          <Content>
            <View style={styles.viewBox}>
                <View>
                  <TouchableOpacity style={styles.viewMenu} onPress={()=> pushScreen(this.props.componentId,'Transaction')} >
                    <Image source={require('../images/wallet-1.png')} style={{width: 50, height: 50}} />
                    <Text style={{textAlign: 'center', fontSize: 12, fontWeight: 'bold'}}>Detail Transaksi</Text>
                  </TouchableOpacity>
                </View>
                <View>
                  <TouchableOpacity style={styles.viewMenu} onPress={()=> pushScreen(this.props.componentId,'Cash')}>
                    <Image source={require('../images/wallet-2.png')} style={{width: 50, height: 50}} />
                    <Text style={{textAlign: 'center', fontSize: 12, fontWeight: 'bold'}}>Tabungan</Text>
                  </TouchableOpacity>
                </View>
                <View>
                  <TouchableOpacity style={styles.viewMenu} onPress={()=> pushScreen(this.props.componentId,'Program')}>
                    <Image source={require('../images/wallet-3.png')} style={{width: 50, height: 50}} />
                    <Text style={{textAlign: 'center', fontSize: 12, fontWeight: 'bold'}}>Program Nasabah</Text>
                  </TouchableOpacity>
                </View>
            </View>

            <View style={styles.viewForm}>  
                <Form>
                  <Item stackedLabel success >
                    <Label style={{fontSize: 13}}>No Rekening</Label>
                    <Input style={{fontSize: 14}}/>
                  </Item>
                  <Item stackedLabel >
                    <Label style={{fontSize: 13}}>Nama Pemilik</Label>
                    <Input style={{fontSize: 14}}/>
                  </Item>
                </Form>
            </View>

            <View>
              <View style={styles.viewTittletime}>
                  <Text style={{color: 'white', fontSize: 13}}>Status</Text>
              </View>

              <View style={styles.viewBoxTime}>
                <View style={{flexDirection: 'row'}}>
                  <View style={{width: 14, height: 15, borderRadius: 100, backgroundColor: 'maroon'}} />
                  <Text style={{marginLeft: 10, fontWeight: 'bold'}}>08.00 WIB</Text>
                </View>
                  <View style={styles.viewKet}>
                      <Text style={{marginLeft: 10}}>Pak Kasep akan menuju kesana setelah makan siang, mohon untuk tidak meninggalkan tempat.</Text>
                  </View>
              </View>

              <View style={styles.viewBoxTime}>
                <View style={{flexDirection: 'row'}}>
                  <View style={{width: 14, height: 15, borderRadius: 100, backgroundColor: 'maroon'}} />
                  <Text style={{marginLeft: 10, fontWeight: 'bold'}}>08.00 WIB</Text>
                </View>
                  <View style={styles.viewKet}>
                      <Text style={{marginLeft: 10}}>Pak Kasep akan menuju kesana setelah makan siang, mohon untuk tidak meninggalkan tempat.</Text>
                  </View>
              </View>
            </View>

            <View style={styles.viewButton}>
                <Button style={{width: 100, borderRadius: 5, elevation: 5, justifyContent: 'center'}}>
                  <Text style={{fontSize: 13, color: 'white', fontWeight: 'bold'}}>Setor Tunai</Text>
                </Button>
                <Button style={{width: 100, borderRadius: 5, elevation: 5, justifyContent: 'center', backgroundColor: 'grey'}}>
                  <Text style={{fontSize: 13, color: 'white', fontWeight: 'bold'}}>Batal Setor</Text>
                </Button>
            </View>
          </Content>
    );
  }

  _renderCek() {
    return (
          <Content>
            <View style={{justifyContent: 'center', alignItems: 'center', padding: 20, marginTop: '50%'}}>
                <Text style={{fontSize: 15, textAlign: 'center'}}>Anda belum memasukan `PIN`, silahkan tekan tombol di bawah ini. . </Text>
                <Button onPress={()=> pushScreen(this.props.componentId, 'PinInput')} style={{width: 150, height: 45, marginTop: 25, borderRadius: 5, alignItems: 'center', justifyContent: 'center'}}>
                    <Text style={{fontSize: 15, color: 'white'}}>PIN</Text>
                </Button>
            </View>
          </Content>
    );
  }


  render() {
    return (
      <Container> 
          {
              this.state.key == '' ?
                this._renderCek()
              : 
               this._renderItems()
          }
          
      </Container>
    
    );
  }
}

const styles = StyleSheet.create({

    viewBox:{
      flexWrap: 'wrap',
      marginLeft: 20,
      marginTop: 20,
      marginRight: 10,
      marginBottom: 20,
      flexDirection: 'row',
      justifyContent: 'space-between'
    },
        viewMenu:{
          backgroundColor: 'white',
          width: 100,
          height: 100,
          borderRadius: 5,
          elevation: 5,
          margin: 5,
          alignItems: 'center',
          justifyContent: 'space-between',
          padding: 10
        },
    viewForm:{
      width: '90%',
      //backgroundColor: 'grey',
      alignSelf: 'center',
      paddingRight: 20,
      paddingBottom: 20
    },

    viewBell:{
      backgroundColor: 'grey',
      width: 100,
      height: 100,
      alignSelf: 'center',
      position: 'absolute',
      bottom: 10,
      borderRadius: 20,
      padding: 10,
      alignItems: 'center'        
    },

    viewTogo:{
      width: '90%',
     // height: 100,
      //backgroundColor: 'grey',
      alignSelf: 'center',
      alignItems: 'center',
      padding: 5,
    },
        viewTime:{
          width: '98%',
         // height: 50,
          backgroundColor: 'white',
          padding: 5,
          justifyContent: 'center',
          alignItems: 'center',
          marginBottom: 10,
          borderRadius: 10,
          elevation: 5
        },
            textJam:{
              fontSize: 15,
              fontWeight: 'bold',
              color: 'red',
              marginBottom: 5
            },


    viewTittletime:{
      backgroundColor: 'maroon',
      alignSelf: 'center',
      padding: 5,
      borderRadius: 5
    },
    viewBoxTime:{
      // backgroundColor: 'gr?ey',
      width: '100%',
      paddingLeft: 15,
      paddingRight: 15,
      marginBottom: 5
    },
        viewKet:{
          width: '100%',
          borderLeftWidth: 4,
          borderColor: 'grey',
          marginLeft: 5,
          marginTop: 5,
          // backgroundColor: 'grey'
        },

    viewButton:{
      justifyContent: 'space-around',
      width: '90%',
      flexDirection: 'row',
      marginTop: 10,
      alignSelf: 'center'
    }

});