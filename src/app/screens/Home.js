import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Image, ImageBackground, ScrollView, TouchableOpacity} from 'react-native';
import { Container, Content, Button, Tab, Tabs, TabHeading} from 'native-base';
import Icon from 'react-native-vector-icons/Entypo';
import { Navigation } from 'react-native-navigation';

import {pushScreen} from '../control/Control';

export default class Home extends Component {
  constructor(props) {
    super(props);
    console.log('home',props)
    this.state = {};
  }
   
  render() {  
    return (
      <Container>
        <Content>   
            
            <View style={styles.viewHeader}>
                <Text style={styles.textName}>Adam Smith</Text>
                <Text>This Sunday with good weather and good people .</Text>
            </View>

            <View style={styles.viewCard}>
              <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} style={{width: '100%', height: '100%'}}>
                  
                  <View style={styles.viewBox}>
                    <TouchableOpacity>
                        <ImageBackground source={require('../images/haji.png')} style={{width: '100%', height: 180, justifyContent: 'space-between'}} imageStyle={{borderRadius: 10}} >
                          <View style={styles.viewUp}>
                              <Text style={styles.text1}>Tabungan Haji</Text>
                          </View>
                          <View style={styles.viewDown}>
                              <Text style={styles.text2}>The Planning</Text>
                              <Text style={styles.text3}>Rencanakan Menabung untuk Haji sampai dengan 5 tahun kedepan.</Text>
                          </View>
                        </ImageBackground>
                    </TouchableOpacity>
                  </View>

                  <View style={styles.viewBox}>
                    <ImageBackground source={require('../images/zakat.png')} style={{width: '100%', height: 180, justifyContent: 'space-between'}} imageStyle={{borderRadius: 10}} >
                      <View style={styles.viewUp}>
                          <Text style={styles.text1}>Bayar Zakat</Text>
                      </View>
                      <View style={styles.viewDown}>
                          <Text style={styles.text2}>The Planning</Text>
                          <Text style={styles.text3}>Bayarlah Zakat karena ada sebagian harta orang lain.</Text>
                      </View>
                    </ImageBackground>
                  </View>

                  <View style={styles.viewBox}>
                    <ImageBackground source={require('../images/sedekah.png')} style={{width: '100%', height: 180, justifyContent: 'space-between'}} imageStyle={{borderRadius: 10}} >
                      <View style={styles.viewUp}>
                          <Text style={styles.text1}>Sedekah</Text>
                      </View>
                      <View style={styles.viewDown}>
                          <Text style={styles.text2}>The Planning</Text>
                          <Text style={styles.text3}>Membatun anak-anak Yatim Piatu untuk meringakan beban walaupun sedikit</Text>
                      </View>
                    </ImageBackground>
                  </View>

              </ScrollView> 
            </View>

            <Text style={styles.textTitle}>Featured</Text>

            <View style={styles.viewFeatured}>
                <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                    <View style={styles.viewBox2}>
                        <Image style={{width: '100%', height: '100%', borderRadius: 10}} source={require('../images/zakat.png')} />
                    </View>
                   <View style={styles.viewBox2}>
                        <Image style={{width: '100%', height: '100%', borderRadius: 10}} source={require('../images/tabungan.png')} />
                    </View>
                    <View style={styles.viewBox2}>
                        <Image style={{width: '100%', height: '100%', borderRadius: 10}} source={require('../images/sedekah.png')} />
                    </View>
                    <View style={styles.viewBox2}>
                        <Image style={{width: '100%', height: '100%', borderRadius: 10}} source={require('../images/haji.png')} />
                    </View>
                </ScrollView>
            </View>

            <Text style={styles.textTitle}>News</Text>

            <ScrollView horizontal={true} style={{margin: 10}} showsHorizontalScrollIndicator={false}>
              <TouchableOpacity onPress={()=> pushScreen(this.props.componentId,'News')}>
                <View style={styles.viewBox3}>
                    <ImageBackground style={{width: '100%', height: '100%', justifyContent: 'flex-end'}} imageStyle={{borderRadius: 10}} source={require('../images/zakat.png')}>
                        <View style={{padding: 5, backgroundColor: 'grey', borderBottomLeftRadius: 10, borderBottomRightRadius: 10}}>
                            <Text>Belajar Sedekah dengan Ikhlas</Text>
                        </View>
                    </ImageBackground>
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={()=> pushScreen(this.props.componentId,'News')}>
                <View style={styles.viewBox3}>
                    <ImageBackground style={{width: '100%', height: '100%', justifyContent: 'flex-end'}} imageStyle={{borderRadius: 10}} source={require('../images/zakat.png')}>
                        <View style={{padding: 5, backgroundColor: 'grey', borderBottomLeftRadius: 10, borderBottomRightRadius: 10}}>
                            <Text>Belajar Sedekah dengan Ikhlas</Text>
                        </View>
                    </ImageBackground>
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={()=> pushScreen(this.props.componentId,'News')}>
                <View style={styles.viewBox3}>
                    <ImageBackground style={{width: '100%', height: '100%', justifyContent: 'flex-end'}} imageStyle={{borderRadius: 10}} source={require('../images/zakat.png')}>
                        <View style={{padding: 5, backgroundColor: 'grey', borderBottomLeftRadius: 10, borderBottomRightRadius: 10}}>
                            <Text>Belajar Sedekah dengan Ikhlas</Text>
                        </View>
                    </ImageBackground>
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={()=> pushScreen(this.props.componentId,'News')}>
                <View style={styles.viewBox3}>
                    <ImageBackground style={{width: '100%', height: '100%', justifyContent: 'flex-end'}} imageStyle={{borderRadius: 10}} source={require('../images/zakat.png')}>
                        <View style={{padding: 5, backgroundColor: 'grey', borderBottomLeftRadius: 10, borderBottomRightRadius: 10}}>
                            <Text>Belajar Sedekah dengan Ikhlas</Text>
                        </View>
                    </ImageBackground>
                </View>
              </TouchableOpacity>
          
            </ScrollView>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({

  viewHeader:{
    //backgroundColor: 'grey',
    width:'100%',
    //height: 80,
    paddingLeft: 20,
    paddingTop: 10,
    paddingRight: 50,
    paddingBottom: 15,
  },
        textName:{
          fontSize: 20,
          fontWeight: 'bold',
          color: 'black'
        },

  viewCard:{
   // backgroundColor: 'grey',
    height: 200,
    width: '100%',
    marginBottom: 10,
    //justifyContent: 'center',
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10
  },
      viewBox:{
        width: 250,
        height: 180,
       // backgroundColor: 'grey',
        //elevation: 3,
        borderRadius: 10,
        marginRight: 10,
       // justifyContent: 'space-between'
      },
            viewUp:{
              //backgroundColor: 'red',
              width: '100%',
              height: 50,
              borderTopLeftRadius: 10,
              borderTopRightRadius: 10,
              justifyContent: 'center'
            },
                  text1:{
                    fontSize: 20,
                    fontWeight: 'bold',
                    marginLeft: 10,
                    color: 'white'
                  },

            viewDown:{
              //backgroundColor: 'red',
              width: '100%',
              //height: 50,
              borderBottomLeftRadius: 10,
              borderBottomRightRadius: 10,
              padding: 8
            },
                  text2:{
                    fontSize: 15,
                    fontWeight: 'bold'
                  },

    textTitle:{
      marginLeft: 10,
      fontWeight: 'bold',
      fontSize: 20
    },

    viewFeatured:{
      //backgroundColor: 'grey',
      width: '100%',
      height: 120,
      padding: 10
    },
          viewBox2:{
            //backgroundColor: 'red',
            width: 100,
            height: 100,
            borderRadius: 10,
            marginRight: 10
          },
          viewBox3:{
            width: 150,
            height: 100,
            marginRight: 10
          }

});
