'use strict';

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  ScrollView
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import { Navigation } from 'react-native-navigation';

class Transaction extends Component {
  render() {
    return (
    <View>
      <View style={styles.viewHeader}>
      	<TouchableOpacity onPress={()=>Navigation.pop(this.props.componentId)}> 
      		<Icon name="arrowleft" style={{fontSize: 30}}/>
      	</TouchableOpacity>
      	<Text style={{fontSize: 15, color: 'black', fontWeight: 'bold'}}>Detail Transaction</Text>
      </View>

      <ScrollView>

      		<View style={{flexDirection: 'row', justifyContent: 'space-between', paddingRight: 15}}>
      			<Text style={{marginLeft: 15, fontSize: 15, marginBottom: 10}}>Transaction List</Text>
      			<Text style={{fontSize: 15, fontWeight: 'bold', marginBottom: 10}}>November 2019</Text>
      		</View>
	      
	      <View style={styles.viewCard}>
	      	<View style={{flexDirection: 'row', alignItems: 'center'}}>
	      		<Icon name="pluscircleo" style={{fontSize: 25, color: 'blue'}}/>
	      		<View style={{marginLeft: 15}}>
	      			<Text style={{fontSize: 14, fontWeight: 'bold', color:'black'}}>TopUp</Text>
	      			<Text style={{fontSize: 12}}>10 November 15:55</Text>
	      		</View>
	      	</View>
	      	<View>
	      		<Text style={{fontSize: 13, color: 'black'}}>200.000,-</Text>
	      	</View>
	      </View>

	      <View style={styles.viewCard}>
	      	<View style={{flexDirection: 'row', alignItems: 'center'}}>
	      		<Icon name="minuscircleo" style={{fontSize: 25, color: 'maroon'}}/>
	      		<View style={{marginLeft: 15}}>
	      			<Text style={{fontSize: 14, fontWeight: 'bold', color:'black'}}>Withdraw</Text>
	      			<Text style={{fontSize: 12}}>10 November 15:55</Text>
	      		</View>
	      	</View>
	      	<View>
	      		<Text style={{fontSize: 13, color: 'black'}}>200.000,-</Text>
	      	</View>
	      </View>

	      <View style={styles.viewCard}>
	      	<View style={{flexDirection: 'row', alignItems: 'center'}}>
	      		<Icon name="upload" style={{fontSize: 25, color: 'maroon'}}/>
	      		<View style={{marginLeft: 15}}>
	      			<Text style={{fontSize: 14, fontWeight: 'bold', color:'black'}}>Sent</Text>
	      			<Text style={{fontSize: 12}}>10 November 15:55</Text>
	      		</View>
	      	</View>
	      	<View>
	      		<Text style={{fontSize: 13, color: 'black'}}>200.000,-</Text>
	      	</View>
	      </View>

	      <View style={styles.viewCard}>
	      	<View style={{flexDirection: 'row', alignItems: 'center'}}>
	      		<Icon name="download" style={{fontSize: 25, color: 'maroon'}}/>
	      		<View style={{marginLeft: 15}}>
	      			<Text style={{fontSize: 14, fontWeight: 'bold', color:'black'}}>Received</Text>
	      			<Text style={{fontSize: 12}}>10 November 15:55</Text>
	      		</View>
	      	</View>
	      	<View>
	      		<Text style={{fontSize: 13, color: 'black'}}>200.000,-</Text>
	      	</View>
	      </View>

      </ScrollView>
    </View>
    );
  }
}

const styles = StyleSheet.create({

	viewHeader:{
		width: '100%',
		height: 50,
		backgroundColor: 'white',
		elevation: 5,
		marginBottom: 10,
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		paddingRight: 15,
		paddingLeft: 15
	},
	viewCard:{
		width: '90%',
		flexDirection: 'row',
		justifyContent: 'space-between',
		backgroundColor: 'white',
		alignSelf: 'center',
		padding: 10,
		elevation: 5,
		marginBottom: 5,
		borderRadius: 5,
		alignItems: 'center'
	}

});


export default Transaction;