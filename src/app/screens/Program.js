'use strict';

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  Image,
  ImageBackground
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import { Navigation } from 'react-native-navigation';

class Program extends Component {

  render() {
    return (
      <View>
    	   <View style={styles.viewHeader}>
	      	<TouchableOpacity onPress={()=>Navigation.pop(this.props.componentId)}> 
	      		<Icon name="arrowleft" style={{fontSize: 30}}/>
	      	</TouchableOpacity>
	      	<Text style={{fontSize: 15, color: 'black', fontWeight: 'bold'}}>My Program</Text>
      	</View>

      	<View style={{justifyContent: 'center', alignItems: 'center', padding: 40, marginTop: 50}}>
      		<Image source={require('../images/haji.png')} style={{width: '60%', height: 150}}/>
      		<Text style={{fontWeight: 'bold', fontSize: 25, marginBottom: 5}}>No Program</Text>
      		<Text>There is no program found to add in home</Text>

      	</View>
      </View>
    );
  }
}

const styles = StyleSheet.create({

	viewHeader:{
		width: '100%',
		height: 50,
		backgroundColor: 'white',
		elevation: 5,
		marginBottom: 10,
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		paddingRight: 15,
		paddingLeft: 15
	},

});


export default Program;