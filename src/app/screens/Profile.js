import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Image, ImageBackground, TouchableOpacity} from 'react-native';
import { Container, Content, Icon, Item, Input, Button,Form, Label} from 'native-base';
import { Navigation } from 'react-native-navigation';

export default class Detail extends Component {
  render() {
    return (
      <Container> 
        <ImageBackground style={{width: '100%', height: '100%'}} source={require('../images/profile.jpg')}>
          <View style={{justifyContent: 'flex-end', height: '100%', width: '100%'}}>
              <View style={styles.viewProfile}>
                  <View style={styles.viewPhoto}>
                    <TouchableOpacity>
                      <Icon name="ios-camera" style={{color: 'white', fontSize: 60}}/>
                    </TouchableOpacity>
                  </View>
                  <Text style={styles.text_1}>Adam Smith</Text>
                  <Text style={styles.text_2}>Bekasi</Text>
                  <View style={{width: '80%'}}>
                      <Text style={styles.text_3}>Freelace UI Designer focusing  on website design & development, and application design.</Text>
                  </View>
              </View>
          </View>
        </ImageBackground>
      </Container>
    
    );
  }
}

const styles = StyleSheet.create({
    viewProfile:{
      width: '100%',
      backgroundColor: 'rgba(255,255,255,0.2)',
      alignItems: 'center',
      padding: 10
    },
        text_1:{
          fontSize: 30,
          marginBottom: 5,
          color: 'white'
        },
        text_2:{
          fontSize: 15,
          color: 'white',
          marginBottom: 5
        },
        text_3:{
          fontSize: 13,
          color: 'rgb(255,255,255)',
          textAlign: 'center'
        },
        viewPhoto:{
          position: 'absolute',
          // backgroundColor: 'red',
          height: 50,
          width: 50,
          top: 0,
          alignSelf: 'flex-end',
          right: 10,
          top: -25,
          justifyContent: 'center',
          alignItems: 'center'
        }


});

