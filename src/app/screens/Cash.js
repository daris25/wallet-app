'use strict';

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  ScrollView
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import { Navigation } from 'react-native-navigation';

class Cash extends Component {
  render() {
    return (
    <View>
      <View style={styles.viewHeader}>
      	<TouchableOpacity onPress={()=>Navigation.pop(this.props.componentId)}> 
      		<Icon name="arrowleft" style={{fontSize: 30}}/>
      	</TouchableOpacity>
      	<Text style={{fontSize: 15, color: 'black', fontWeight: 'bold'}}>My Cash</Text>
      </View>
      <ScrollView>
      	<Text style={{marginLeft: 15, fontSize: 15, color: 'black'}}>Your Wallet</Text>
      		<View style={styles.viewCard}>
      			<Text style={{fontSize: 13}}>Avalibel Belance</Text>
      			<Text style={{fontSize: 30, fontWeight: 'bold'}}>Rp 200.500.000.000</Text>
      			<View style={{justifyContent: 'space-between', marginTop: 30, flexDirection: 'row'}}>
	      			<Text style={{fontSize: 15, fontWeight: 'bold'}}>Adam Smith</Text>
	      			<Text>No.Rek : 123123994941</Text>
      			</View>
      		</View>

      	<View style={{flexDirection: 'row', padding: 15, flexWrap: 'wrap'}}>
  			<TouchableOpacity style={styles.viewSer}>
      			<Icon name="upload" style={{fontSize: 25}}/>
      			<Text style={{textAlign: 'center', fontSize: 11, marginTop: 5}}>Transef Sesama</Text>
  			</TouchableOpacity>

      	</View>


      </ScrollView>
    </View>
    );
  }
}

const styles = StyleSheet.create({

	viewHeader:{
		width: '100%',
		height: 50,
		backgroundColor: 'white',
		elevation: 5,
		marginBottom: 10,
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		paddingRight: 15,
		paddingLeft: 15
	},

	viewCard:{
		backgroundColor: 'white',
		width: '90%',
		height: 150,
		marginTop: 10,
		alignSelf: 'center',
		borderRadius: 5,
		justifyContent: 'center',
		elevation: 5,
		marginBottom: 10,
		padding: 15
	},

	viewSer:{
		width: '23.4848%',
		height: 80,
		backgroundColor: 'white',
		borderRadius: 5,
		marginRight: 5,
		justifyContent: 'center',
		alignItems: 'center',
		elevation: 5
	}

});


export default Cash;