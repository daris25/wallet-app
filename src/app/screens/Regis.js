'use strict';

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text
} from 'react-native';
import { Item, Input, Button, Form, Label} from 'native-base';
import Icon from 'react-native-vector-icons/AntDesign';
import { Navigation } from 'react-native-navigation';
import {pushScreen} from '../control/Control';

class Regis extends Component {
  render() {
    return (
    <View>
      <View style={styles.viewHeader}>
      	<TouchableOpacity onPress={()=>Navigation.pop(this.props.componentId)}> 
      		<Icon name="arrowleft" style={{fontSize: 30}}/>
      	</TouchableOpacity>
      	<Text style={{fontSize: 15, color: 'black', fontWeight: 'bold'}}>Registrasi</Text>
      </View>

      <View style={{padding: 10}}>
      		<View style={{paddingLeft: 15, paddingRight: 15}}>
		        <Item floatingLabel style={{marginBottom: 10}}>
		          <Label style={{fontSize: 13}}>Email</Label>
		          <Input style={{fontSize: 14}}/>
		        </Item>
		    </View>
		    <View style={{paddingLeft: 15, paddingRight: 15}}>
		        <Item floatingLabel style={{marginBottom: 10}}>
		          <Label style={{fontSize: 13}}>No Handphone</Label>
		          <Input style={{fontSize: 14}}/>
		        </Item>
		    </View>
      		<View style={{paddingLeft: 15, paddingRight: 15}}>
		        <Item floatingLabel style={{marginBottom: 10}}>
		          <Label style={{fontSize: 13}}>No Tabungan</Label>
		          <Input style={{fontSize: 14}}/>
		        </Item>
		    </View>
		  	<View style={{paddingLeft: 15, paddingRight: 15}}>
		        <Item floatingLabel style={{marginBottom: 10}}>
		          <Label style={{fontSize: 13}}>Username</Label>
		          <Input style={{fontSize: 14}}/>
		        </Item>
		    </View>
		    <View style={{paddingLeft: 15, paddingRight: 15}}>
		        <Item floatingLabel style={{marginBottom: 10}}>
		          <Label style={{fontSize: 13}}>Password</Label>
		          <Input style={{fontSize: 14}}/>
		        </Item>
		    </View>
		    <View style={{paddingLeft: 15, paddingRight: 15}}>
		        <Item floatingLabel style={{marginBottom: 10}}>
		          <Label style={{fontSize: 13}}>Repassword</Label>
		          <Input style={{fontSize: 14}}/>
		        </Item>
		    </View>
	   </View>

	   <Button style={{width: '90%', alignSelf: 'center', justifyContent: 'center', borderRadius: 5, marginTop: 15}}>
	   		<Text style={{fontSize: 14, color: 'white', textAlign: 'center'}}>Sign Up</Text>
	   </Button>
     

    </View>
    );
  }
}

const styles = StyleSheet.create({

	viewHeader:{
		width: '100%',
		height: 50,
		backgroundColor: 'white',
		elevation: 5,
		marginBottom: 10,
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		paddingRight: 15,
		paddingLeft: 15
	},

});


export default Regis;