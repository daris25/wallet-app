import {observable} from 'mobx'
import {AsyncStorage} from 'react-native'

import config from '../config'
import FetchApi from '../utils/FetchApi'
import encodeURI from '../utils/EncodeURI'

class AuthStore {

  //@observable isLoading = false;
  //@observable isLogin = false;
  // @observable token = null;
  // @observable loginUser = null;
  // @observable profileComp = null;
  // @observable loginEmp = null;
  
  constructor() {
    this.fetch = new FetchApi(config.base_url_local);
  }


}

const authStore = new AuthStore()
export default authStore